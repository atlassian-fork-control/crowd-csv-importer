package com.atlassian.crowd.csvimporter.parsers.exceptions;

public class CsvParsingException extends Exception
{
    public CsvParsingException(String s)
    {
        super(s);
    }
}
