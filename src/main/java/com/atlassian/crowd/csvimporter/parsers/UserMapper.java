package com.atlassian.crowd.csvimporter.parsers;

import com.atlassian.crowd.csvimporter.parsers.exceptions.CsvParsingException;
import com.atlassian.crowd.integration.rest.entity.PasswordEntity;
import com.atlassian.crowd.integration.rest.entity.UserEntity;

public class UserMapper extends AbstractCsvMapper<UserEntity>
{
    @Override
    public String[] expectedHeader()
    {
        return new String[]{
            "Username", "First Name", "Last Name", "Email Address", "Password"
        };
    }

    @Override
    public UserEntity generate(String[] csvLine) throws CsvParsingException
    {
        if (csvLine.length != 5)
        {
            throw new CsvParsingException("Expecting exactly 5 columns but received " + csvLine.length);
        }

        final String firstName = csvLine[1];
        final String lastName = csvLine[2];
        final String displayName = (firstName + " " + lastName).trim();
        return new UserEntity(csvLine[0].trim(), firstName, lastName, displayName, csvLine[3].trim(), new PasswordEntity(csvLine[4]),
            true);
    }
}
