package com.atlassian.crowd.csvimporter.parsers;

import java.util.Arrays;

public abstract class AbstractCsvMapper<T> implements CsvMapper<T>
{
    @Override
    public boolean headerMatches(String[] possibleHeaderLine)
    {
        for (int i = 0; i < possibleHeaderLine.length; ++i)
        {
            possibleHeaderLine[i] = possibleHeaderLine[i].trim();
        }
        return Arrays.equals(possibleHeaderLine, expectedHeader());
    }
}
