package com.atlassian.crowd.csvimporter.parsers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import com.atlassian.crowd.csvimporter.parsers.exceptions.CsvParsingException;

import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import au.com.bytecode.opencsv.CSVReader;

import static com.atlassian.crowd.csvimporter.AppExitCode.CSV_PARSING_FAILURE;
import static com.atlassian.crowd.csvimporter.AppExitCode.LINE_READING_ERROR;

public class CsvMappingParser<T>
{
    private static final Logger log = LoggerFactory.getLogger(CsvMappingParser.class);

    private final CsvMapper<T> csvMapper;

    public CsvMappingParser(CsvMapper<T> csvMapper)
    {
        this.csvMapper = csvMapper;
    }

    public List<T> parse(File file) throws FileNotFoundException
    {
        try
        {
            final CSVReader csvReader = new CSVReader(new FileReader(file));

            final String[] firstLine = csvReader.readNext();
            if (!csvMapper.headerMatches(firstLine))
            {
                log.error("The header line of the {} CSV file is incorrect. Expected format: {}", file, Arrays.toString(
                    csvMapper.expectedHeader()));
                CSV_PARSING_FAILURE.exit();
            }

            return Lists.newArrayList(Iterables.transform(csvReader.readAll(), new Function<String[], T>()
            {
                @Override
                public T apply(String[] currentLine)
                {
                    try
                    {
                        return csvMapper.generate(currentLine);
                    }
                    catch (CsvParsingException e)
                    {
                        log.error("Failed to parse the CSV file.", e);
                        CSV_PARSING_FAILURE.exit();
                    }
                    return null;
                }
            }));
        }
        catch (IOException e)
        {
            log.error("Failed to read a line in the CSV parser.", e);
            LINE_READING_ERROR.exit();
        }

        return new LinkedList<T>();
    }
}
