package com.atlassian.crowd.csvimporter.callables;

import java.util.concurrent.Callable;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.service.client.CrowdClient;

import com.google.common.base.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The purpose of this callable is to find all of the groups that do not exist.
 */
public class GroupNotExistsCallable implements Callable<Optional<String>>
{
    private static final Logger log = LoggerFactory.getLogger(GroupNotExistsCallable.class);

    private final CrowdClient crowdClient;
    private final String groupName;

    public GroupNotExistsCallable(CrowdClient crowdClient, String groupName)
    {
        this.crowdClient = crowdClient;
        this.groupName = groupName;
    }

    @Override
    public Optional<String> call()
    {
        try
        {
            crowdClient.getGroup(groupName);
            return Optional.absent();
        }
        catch (GroupNotFoundException e)
        {
            return Optional.of(groupName);
        }
        catch (OperationFailedException | InvalidAuthenticationException | ApplicationPermissionException e)
        {
            log.error("Failed to try and see if the group existed: " + groupName, e);
        }

        return Optional.absent();
    }
}
