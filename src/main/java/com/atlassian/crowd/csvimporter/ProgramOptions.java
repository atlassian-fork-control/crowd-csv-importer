package com.atlassian.crowd.csvimporter;

import java.io.File;

import com.google.common.base.Optional;

public class ProgramOptions
{
    private Optional<File> groupFileName = Optional.absent();
    private Optional<File> usersFileName = Optional.absent();
    private int maxHttpThreads = 20;
    private boolean usingEncryptedPasswords;
    private boolean blacklistEnabled = true;
    private CrowdDetails crowdDetails;

    @Override
    public String toString()
    {
        return "ProgramOptions{" +
            "groupFileName=" + groupFileName +
            ", usersFileName=" + usersFileName +
            ", maxHttpThreads=" + maxHttpThreads +
            '}';
    }

    public Optional<File> getGroupFileName()
    {
        return groupFileName;
    }

    public void setGroupFileName(File groupFileName)
    {
        this.groupFileName = Optional.fromNullable(groupFileName);
    }

    public Optional<File> getUsersFileName()
    {
        return usersFileName;
    }

    public void setUsersFileName(File usersFileName)
    {
        this.usersFileName = Optional.fromNullable(usersFileName);
    }

    public int getMaxHttpThreads()
    {
        return maxHttpThreads;
    }

    public void setMaxHttpThreads(int maxHttpThreads)
    {
        this.maxHttpThreads = maxHttpThreads;
    }

    public CrowdDetails getCrowdDetails()
    {
        return crowdDetails;
    }

    public void setCrowdDetails(CrowdDetails crowdDetails)
    {
        this.crowdDetails = crowdDetails;
    }

    public boolean isBlacklistEnabled()
    {
        return blacklistEnabled;
    }

    public void setBlacklistEnabled(boolean blacklistEnabled)
    {
        this.blacklistEnabled = blacklistEnabled;
    }

    public boolean isUsingEncryptedPasswords()
    {
        return usingEncryptedPasswords;
    }

    public void setUsingEncryptedPasswords(boolean usingEncryptedPasswords)
    {
        this.usingEncryptedPasswords = usingEncryptedPasswords;
    }
}
