package com.atlassian.crowd.csvimporter;

import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IterableUtils
{
    private static final Logger log = LoggerFactory.getLogger(IterableUtils.class);

    public static <A> Iterable<Future<A>> submitAll(ExecutorService executorService,
                                                    Iterable<? extends Callable<A>> callables)
    {
        final List<Future<A>> futures = new LinkedList<Future<A>>();
        for (Callable<A> callable : callables)
        {
            futures.add(executorService.submit(callable));
        }
        return futures;
    }

    public static <A> Iterable<A> flushAll(Iterable<Future<A>> futures)
    {
        final List<A> results = new LinkedList<A>();
        for (Future<A> future : futures)
        {
            try
            {
                results.add(future.get());
            }
            catch (InterruptedException e)
            {
                log.error("Could not get future back.", e);
            }
            catch (ExecutionException e)
            {
                log.error("Future failed to execute.", e);
            }
        }
        return results;
    }

    public static int countTrue(Iterable<Boolean> results)
    {
        return Iterables.size(Iterables.filter(results, new Predicate<Boolean>()
        {
            @Override
            public boolean apply(Boolean succeeded)
            {
                return succeeded;
            }
        }));
    }
}
