package com.atlassian.crowd.csvimporter;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.atlassian.crowd.csvimporter.parsers.CsvMappingParser;
import com.atlassian.crowd.csvimporter.parsers.entities.GroupMembership;
import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.integration.rest.entity.GroupEntity;
import com.atlassian.crowd.integration.rest.entity.PasswordEntity;
import com.atlassian.crowd.integration.rest.entity.UserEntity;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.service.client.CrowdClient;

import com.google.common.collect.ImmutableList;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatcher;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.argThat;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AppTest
{
    private ProgramOptions programOptions;
    private EndResults endResults;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    @Mock
    private CrowdClient crowdClient;
    @Mock
    private File usersFile;
    @Mock
    private File groupsFile;
    @Mock
    private CsvMappingParser<UserEntity> userParser;
    @Mock
    private CsvMappingParser<GroupMembership> groupParser;

    @Before
    public void init() throws IOException
    {
        this.programOptions = new ProgramOptions();
        programOptions.setUsersFileName(usersFile);
        programOptions.setGroupFileName(groupsFile);
        programOptions.setCrowdDetails(new CrowdDetails("hostname", "application", "password", null, false));
        when(usersFile.canRead()).thenReturn(true);
        when(groupsFile.canRead()).thenReturn(true);
        this.endResults = new EndResults();
    }

    @Test
    public void shouldNotTryToProcessUsersIfNoUsersFileIsSpecified()
    {
        programOptions.setUsersFileName(null);
        final AppExitCode appExitCode = App.handleUsersFile(programOptions, crowdClient, executorService, userParser,
            endResults);

        assertEquals("Should skip users processing if no users file is specified", AppExitCode.SUCCESS, appExitCode);

        verifyZeroInteractions(crowdClient);
    }

    @Test
    public void shouldFailIfUsersFileIsSpecifiedButIsNotReadable()
    {
        when(usersFile.canRead()).thenReturn(false);
        final AppExitCode appExitCode = App.handleUsersFile(programOptions, crowdClient, executorService, userParser,
            endResults);

        assertEquals("Should fail with appropriate exit code if users file is specified but not readable",
            AppExitCode.USERS_FILE_NOT_EXIST, appExitCode);

        verifyZeroInteractions(crowdClient);
    }

    @Test
    public void shouldCreateUserForEachUserListedInUsersFile() throws Exception
    {
        final int numberOfUsersInFile = 10;
        when(userParser.parse(usersFile)).thenReturn(createUsers(numberOfUsersInFile));

        final AppExitCode appExitCode = App.handleUsersFile(programOptions, crowdClient, executorService, userParser,
            endResults);

        assertEquals("Should have successfully processed the users", AppExitCode.SUCCESS, appExitCode);
        verifyUsersWereCreated(numberOfUsersInFile);
    }

    @Test
    public void shouldNotTryToProcessGroupsIfNoGroupsFileIsSpecified()
    {
        programOptions.setGroupFileName(null);

        final AppExitCode appExitCode = App.handleGroupsFile(programOptions, crowdClient, executorService, groupParser,
            endResults);

        assertEquals("Should skip groups processing if no groups file is specified", AppExitCode.SUCCESS, appExitCode);
        verifyZeroInteractions(crowdClient);
    }

    @Test
    public void shouldFailIfGroupsFileIsSpecifiedButIsNotReadable()
    {
        when(groupsFile.canRead()).thenReturn(false);

        final AppExitCode appExitCode = App.handleGroupsFile(programOptions, crowdClient, executorService, groupParser,
            endResults);

        assertEquals("Should fail with appropriate exit code if groups file is specified but not readable",
            AppExitCode.GROUPS_FILE_NOT_EXIST, appExitCode);
        verifyZeroInteractions(crowdClient);
    }

    @Test
    public void shouldExecuteAPostForEachMembershipListedInGroupsFile() throws Exception
    {
        final int numberOfMembershipsInFile = 10;

        when(groupParser.parse(groupsFile)).thenReturn(createGroupMemberships(numberOfMembershipsInFile));

        final AppExitCode appExitCode = App.handleGroupsFile(programOptions, crowdClient, executorService, groupParser,
            endResults);

        assertEquals("Should have successfully processed the group memberships", AppExitCode.SUCCESS, appExitCode);
        verifyMembershipsWereAdded(numberOfMembershipsInFile);
    }

    @Test
    public void shouldCreateMissingGroups() throws Exception
    {
        final int numberOfMembershipsInFile = 10;
        when(groupParser.parse(groupsFile)).thenReturn(createGroupMemberships(numberOfMembershipsInFile));
        final ImmutableList<Integer> missingGroups = ImmutableList.of(2, 4, 6, 7);
        for (Integer missingGroupId : missingGroups)
        {
            final String groupName = makeGroupName(missingGroupId);
            when(crowdClient.getGroup(groupName)).thenThrow(new GroupNotFoundException(groupName));
        }

        final AppExitCode appExitCode = App.handleGroupsFile(programOptions, crowdClient, executorService, groupParser,
            endResults);

        assertEquals("Should have successfully processed the group memberships", AppExitCode.SUCCESS, appExitCode);
        for (Integer missingGroupId : missingGroups)
        {
            final String groupName = makeGroupName(missingGroupId);
            verify(crowdClient).addGroup(new GroupEntity(groupName, groupName, GroupType.GROUP, true));
        }
        verifyMembershipsWereAdded(numberOfMembershipsInFile);
    }

    @Test
    public void shouldIgnoreBlackListedGroups() throws Exception
    {
        final List<GroupMembership> groupMemberships = createGroupMemberships(5);
        final ImmutableList.Builder<GroupMembership> builder = ImmutableList.<GroupMembership>builder().addAll(
            groupMemberships);
        for (String blackListedGroup : App.BLACKLISTED_GROUPS)
        {
            builder.add(new GroupMembership(makeUsername(1), blackListedGroup.toUpperCase()));
        }
        when(groupParser.parse(groupsFile)).thenReturn(builder.build());

        final AppExitCode appExitCode = App.handleGroupsFile(programOptions, crowdClient, executorService, groupParser,
            endResults);

        assertEquals("Should have successfully processed the group memberships", AppExitCode.SUCCESS, appExitCode);
        verifyMembershipsWereAdded(5);
        for (String blackListedGroup : App.BLACKLISTED_GROUPS)
        {
            verify(crowdClient, never()).addUserToGroup(anyString(), eq(blackListedGroup));
        }
    }

    private void verifyMembershipsWereAdded(int numberOfMembershipsInFile) throws Exception
    {
        for (int i = 0; i < numberOfMembershipsInFile; i++)
        {
            verify(crowdClient).addUserToGroup(makeUsername(i), makeGroupName(i));
        }
    }

    private void verifyUsersWereCreated(int numberOfUsersInFile) throws Exception
    {
        for (int i = 0; i < numberOfUsersInFile; i++)
        {
            final int userIndex = i;
            verify(crowdClient, times(1)).addUser(argThat(new ArgumentMatcher<UserEntity>()
            {
                @Override
                public boolean matches(Object argument)
                {
                    UserEntity userEntity = (UserEntity) argument;
                    return userEntity.getName().equals(makeUsername(userIndex));
                }
            }), argThat(new ArgumentMatcher<PasswordCredential>()
            {
                @Override
                public boolean matches(Object argument)
                {
                    return makePassword(userIndex).getValue().equals(((PasswordCredential) argument).getCredential());
                }
            }));
        }
    }

    private static List<UserEntity> createUsers(int n)
    {
        final ImmutableList.Builder<UserEntity> builder = ImmutableList.builder();
        for (int i = 0; i < n; i++)
        {
            final String firstName = "first";
            final String lastname = "last";
            final String displayName = firstName + " " + lastname;
            builder.add(new UserEntity(makeUsername(i), "first", "last", displayName, "email@test.com", makePassword(i),
                true));
        }
        return builder.build();
    }

    private static List<GroupMembership> createGroupMemberships(int n)
    {
        final ImmutableList.Builder<GroupMembership> builder = ImmutableList.builder();
        for (int i = 0; i < n; i++)
        {
            builder.add(new GroupMembership(makeUsername(i), makeGroupName(i)));
        }
        return builder.build();
    }

    private static String makeGroupName(int i)
    {
        return "group" + i;
    }

    private static String makeUsername(int i)
    {
        return "user" + i;
    }

    private static PasswordEntity makePassword(int i)
    {
        return new PasswordEntity("pwd" + i);
    }
}
